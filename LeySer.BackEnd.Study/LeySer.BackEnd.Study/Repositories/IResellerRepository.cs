﻿using LeySer.BackEnd.Study.LeySerManagementDbModels;

namespace LeySer.BackEnd.Study.Repositories
{
    public interface IResellerRepository
    {
        /// <summary>
        /// 获取所有贩卖店
        /// </summary>
        /// <returns></returns>
        Task<List<ReSeller>> GetReSellersAsync();

        /// <summary>
        /// 删除指定贩卖店
        /// </summary>
        /// <param name="resellerId"></param>
        /// <returns></returns>
        Task<bool> DeleteResellerAsync(int resellerId);

        /// <summary>
        /// 获取指定贩卖店
        /// </summary>
        /// <param name="resellerId"></param>
        /// <returns></returns>
        Task<ReSeller?> GetResellerByIdAsync(int resellerId);

        /// <summary>
        /// 更新贩卖店信息
        /// </summary>
        /// <param name="reseller"></param>
        /// <returns></returns>
        Task UpdateResellerAsync(ReSeller reseller);

        /// <summary>
        /// 创建贩卖店
        /// </summary>
        /// <param name="reseller"></param>
        /// <returns></returns>
        Task CreateReSellersAsync(ReSeller reseller);
    }
}
