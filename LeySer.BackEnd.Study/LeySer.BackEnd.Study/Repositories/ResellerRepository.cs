﻿using LeySer.BackEnd.Study.DbContexts;
using LeySer.BackEnd.Study.LeySerManagementDbModels;

namespace LeySer.BackEnd.Study.Repositories
{
    public class ResellerRepository : IResellerRepository
    {
        private LeySerManagementDbContext Context;

        public ResellerRepository(LeySerManagementDbContext context)
        {
            Context = context;
        }

        public async Task CreateReSellersAsync(ReSeller reseller)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteResellerAsync(int resellerId)
        {
            throw new NotImplementedException();
        }

        public async Task<ReSeller?> GetResellerByIdAsync(int resellerId)
        {
            throw new NotImplementedException();
        }

        public async Task<List<ReSeller>> GetReSellersAsync()
        {
            throw new NotImplementedException();
        }

        public async Task UpdateResellerAsync(ReSeller reseller)
        {
            throw new NotImplementedException();
        }
    }
}
