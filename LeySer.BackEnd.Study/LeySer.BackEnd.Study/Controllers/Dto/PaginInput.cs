﻿namespace LeySer.BackEnd.Study.Controllers.Dto
{
    public class PaginInput
    {
        public int PageSize { get; set; } = 50;

        public int PageIndex { get; set; } = 1;
    }
}
