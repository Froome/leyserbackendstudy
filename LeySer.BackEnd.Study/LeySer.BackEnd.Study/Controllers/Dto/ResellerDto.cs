﻿namespace LeySer.BackEnd.Study.Controllers.Dto
{
    public class ResellerDto
    {
        public int Id { get; set; }
        public int Code { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string Memo { get; set; } = string.Empty;

    }
}
