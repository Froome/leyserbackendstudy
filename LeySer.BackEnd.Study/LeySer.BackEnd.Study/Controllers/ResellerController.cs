﻿using LeySer.BackEnd.Study.Controllers.Dto;
using LeySer.BackEnd.Study.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LeySer.BackEnd.Study.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ResellerController : ControllerBase
    {
        private IResellerRepository _resellerRepository;
        public ResellerController(IResellerRepository resellerRepository)
        {
            _resellerRepository = resellerRepository;
        }

        [HttpPost]
        public async Task<List<ResellerDto>> GetResellerListData(PaginInput input)
        {
            //Demo
            var resellers = await _resellerRepository.GetReSellersAsync();

            throw new NotImplementedException();
        }

        [HttpGet]
        public async Task<ResellerDto> GetResellerDataById(int resellerId)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public async Task<bool> DeleteReseller(int resellerId)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public async Task<bool> UpdateReseller(ResellerDto input)
        {
            throw new NotImplementedException();

        }

        [HttpPost]
        public async Task<bool> CreateReseller(ResellerDto input)
        {
            throw new NotImplementedException();
        }

    };
}
