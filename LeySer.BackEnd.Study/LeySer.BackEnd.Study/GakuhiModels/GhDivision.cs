﻿using System;
using System.Collections.Generic;

namespace LeySer.BackEnd.Study.GakuhiModels
{
    public partial class GhDivision
    {
        public GhDivision()
        {
            InverseParentDivi = new HashSet<GhDivision>();
        }

        public int Id { get; set; }
        public int DiviCode { get; set; }
        public string DiviName { get; set; } = null!;
        public string DiviShortName { get; set; } = null!;
        public int DiviDisplayOrder { get; set; }
        public short DiviDisable { get; set; }
        public string? DiviDesc { get; set; }
        public short IsSchoolFlag { get; set; }
        public string? SchoolCode { get; set; }
        public string? DiviKey { get; set; }
        public int? ParentDiviId { get; set; }
        public string? DiviPath { get; set; }
        public string CreateUser { get; set; } = null!;
        public DateTime CreateDate { get; set; }
        public string ChangeUser { get; set; } = null!;
        public DateTime ChangeDate { get; set; }
        public int ChangeNum { get; set; }
        public int? UpgradeDiviId { get; set; }
        public string? DiviPrintName { get; set; }
        public string? DiviFullName { get; set; }
        public string? DiviFullShortName { get; set; }
        public string? DiviFullPrintName { get; set; }

        public virtual GhDivision? ParentDivi { get; set; }
        public virtual ICollection<GhDivision> InverseParentDivi { get; set; }
    }
}
