﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using LeySer.BackEnd.Study.LeySerManagementDbModels;

namespace LeySer.BackEnd.Study.DbContexts
{
    public partial class LeySerManagementDbContext : DbContext
    {
        public LeySerManagementDbContext()
        {
        }

        public LeySerManagementDbContext(DbContextOptions<LeySerManagementDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ReSeller> ReSellers { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=xa-lsr-nextdev;Database=LeySerManagement;User Id=sa;Password=xA123456;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ReSeller>(entity =>
            {
                entity.HasKey(e => e.ResellerId)
                    .HasName("PK_Reseller10")
                    .IsClustered(false);

                entity.ToTable("ReSeller");

                entity.Property(e => e.ResellerId).ValueGeneratedNever();

                entity.Property(e => e.Address1).HasMaxLength(30);

                entity.Property(e => e.Address2).HasMaxLength(30);

                entity.Property(e => e.Address3).HasMaxLength(30);

                entity.Property(e => e.Contact).HasMaxLength(30);

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Fax).HasMaxLength(20);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.PivotalId)
                    .HasMaxLength(8)
                    .HasColumnName("PivotalID")
                    .HasDefaultValueSql("(0)")
                    .IsFixedLength();

                entity.Property(e => e.ResellerName).HasMaxLength(30);

                entity.Property(e => e.TheMemo).HasMaxLength(800);

                entity.Property(e => e.Zip)
                    .HasMaxLength(10)
                    .HasColumnName("ZIP");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
