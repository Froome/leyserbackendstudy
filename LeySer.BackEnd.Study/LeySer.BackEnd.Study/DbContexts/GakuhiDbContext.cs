﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using LeySer.BackEnd.Study.GakuhiModels;

namespace LeySer.BackEnd.Study.DbContexts
{
    public partial class GakuhiDbContext : DbContext
    {
        public GakuhiDbContext()
        {
        }

        public GakuhiDbContext(DbContextOptions<GakuhiDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<GhDivision> GhDivisions { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=xa-lsr-nextdev;Database=Account_000002_Gakuhi_20210624100822;User Id=sa;Password=xA123456;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GhDivision>(entity =>
            {
                entity.ToTable("GH_DIVISION");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.ChangeDate)
                    .HasColumnType("datetime")
                    .HasColumnName("change_date");

                entity.Property(e => e.ChangeNum).HasColumnName("change_num");

                entity.Property(e => e.ChangeUser)
                    .HasMaxLength(30)
                    .HasColumnName("change_user");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasColumnName("create_date");

                entity.Property(e => e.CreateUser)
                    .HasMaxLength(30)
                    .HasColumnName("create_user");

                entity.Property(e => e.DiviCode).HasColumnName("divi_code");

                entity.Property(e => e.DiviDesc)
                    .HasMaxLength(128)
                    .HasColumnName("divi_desc");

                entity.Property(e => e.DiviDisable).HasColumnName("divi_disable");

                entity.Property(e => e.DiviDisplayOrder).HasColumnName("divi_display_order");

                entity.Property(e => e.DiviFullName)
                    .HasMaxLength(232)
                    .HasColumnName("divi_full_name")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DiviFullPrintName)
                    .HasMaxLength(232)
                    .HasColumnName("divi_full_print_name")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DiviFullShortName)
                    .HasMaxLength(120)
                    .HasColumnName("divi_full_short_name")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.DiviKey)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("divi_key")
                    .IsFixedLength();

                entity.Property(e => e.DiviName)
                    .HasMaxLength(32)
                    .HasColumnName("divi_name");

                entity.Property(e => e.DiviPath)
                    .HasMaxLength(256)
                    .IsUnicode(false)
                    .HasColumnName("divi_path");

                entity.Property(e => e.DiviPrintName)
                    .HasMaxLength(32)
                    .HasColumnName("divi_print_name");

                entity.Property(e => e.DiviShortName)
                    .HasMaxLength(16)
                    .HasColumnName("divi_short_name");

                entity.Property(e => e.IsSchoolFlag).HasColumnName("is_school_flag");

                entity.Property(e => e.ParentDiviId).HasColumnName("parent_divi_id");

                entity.Property(e => e.SchoolCode)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("school_code")
                    .HasDefaultValueSql("('00')")
                    .IsFixedLength();

                entity.Property(e => e.UpgradeDiviId).HasColumnName("upgrade_divi_id");

                entity.HasOne(d => d.ParentDivi)
                    .WithMany(p => p.InverseParentDivi)
                    .HasForeignKey(d => d.ParentDiviId)
                    .HasConstraintName("FK_parent_divi_id@GH_DIVISION");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
