﻿using System;
using System.Collections.Generic;

namespace LeySer.BackEnd.Study.LeySerManagementDbModels
{
    public partial class ReSeller
    {
        public int ResellerId { get; set; }
        public int Code { get; set; }
        public string ResellerName { get; set; } = null!;
        public short Status { get; set; }
        public string Contact { get; set; } = null!;
        public string? Zip { get; set; }
        public string? Address1 { get; set; }
        public string? Address2 { get; set; }
        public string? Address3 { get; set; }
        public string Phone { get; set; } = null!;
        public string Email { get; set; } = null!;
        public short RebateRate { get; set; }
        public string? Fax { get; set; }
        public byte[] PivotalId { get; set; } = null!;
        public string? TheMemo { get; set; }
    }
}
